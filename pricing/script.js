const priceButton = document.querySelectorAll(".price-btn");

Array.from(priceButton).forEach((ele) => {
  ele.addEventListener("click", (e) => {
    document.querySelector(".modal").classList.add("modal-show");
  });
});

document.querySelector(".close").addEventListener("click", () => {
  document.querySelector(".modal").classList.remove("modal-show");
});

document.addEventListener("keydown", (e) => {
  if (e.key === "Escape") {
    document.querySelector(".modal").classList.remove("modal-show");
  }
});

document.getElementById("slider").addEventListener("change", (e) => {
  const priceCards = document.querySelectorAll(".price-card");
  console.log(priceCards);
  const value = Number(e.target.value);
  if (value === 0 || value === 10) {
    priceCards[0].classList.add("highlight");
    priceCards[1].classList.remove("highlight");
    priceCards[2].classList.remove("highlight");
  } else if (value === 20) {
    priceCards[0].classList.remove("highlight");
    priceCards[1].classList.add("highlight");
    priceCards[2].classList.remove("highlight");
  } else {
    priceCards[0].classList.remove("highlight");
    priceCards[1].classList.remove("highlight");
    priceCards[2].classList.add("highlight");
  }
});

async function submitHandler() {
  event.preventDefault();

  const form = document.getElementById("myForm");
  const url = "https://forms.maakeetoo.com/formapi/565";

  const formData = new FormData(form);

  const requestOptions = {
    method: "POST",
    body: formData,
  };

  try {
    await fetch(url, requestOptions)
      .then((response) => {
        alert("Internal Server Error, response not ok");
        return response.json();
      })
      .then((data) => {
        console.log("success", data);
      });
    document.querySelector(".modal").classList.remove("modal-show");
    alert("Form Submitted Successfully");
  } catch (err) {
    document.querySelector(".modal").classList.remove("modal-show");
    alert("Form Submitted Successfully");
  }
}
